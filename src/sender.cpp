//
// Created by tzhou on 11/17/17.
//

#include <zmq.h>
#include <utilities/flags.hpp>
#include <utilities/types.hpp>
#include <cstring>
#include "sender.hpp"
#include "msi.hpp"
#include "threads/listenerThread.hpp"

Sender::Sender() {
    _msi = NULL;
    _signal_cnt = 0;
}

void Sender::initialize() {
    _msi = SysDict::msi;
    for (auto h: _msi->hosts()) {
        if (h.first != SysDict::rank()) {
            _peers[h.first] = h.second;
        }
    }
    init_host_connections();
}

void Sender::init_host_connections() {
    for (auto it: peers()) {
        void* requester = zmq_socket(_msi->context(), ZMQ_REQ);
        it.second->socket = requester;
        zpl("node %d %s, requester: %p", it.first, it.second->name.c_str(), requester)
        zmq_connect(requester, it.second->name.c_str());
    }
}

void Sender::finalize() {
    destroy_host_connections();
}

void Sender::destroy_host_connections() {
    zpl("in Sender::destroy_host_connections")
    for (auto it: peers()) {
        zmq_close(it.second->socket);
        delete it.second;
    }
}

void Sender::send_fetch(PtrRecord* pr) {
    int buf[2];
    buf[0] = MSI::MSG_FETCH;  // message type byte
    buf[1] = pr->id;
    void* requester = peers()[pr->owner]->socket;
    guarantee(requester, "node %d requester: %p", pr->owner, requester);
    if (PrintMessageType) {
        printf("[node %d] -> [node %d](Fetch)\n", _msi->rank(), pr->owner);
    }
    int err = zmq_send(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
}

void Sender::send_invalidate(int node, PtrRecord *pr) {
    int buf[3];
    buf[0] = MSI::MSG_INVALIDATE;
    buf[1] = pr->id;
    buf[2] = pr->owner;
    void* requester = peers()[node]->socket;
    guarantee(requester, "node %d requester: %p", node, requester);
    if (PrintMessageType) {
        printf("[node %d] -> [node %d](Inv)\n", _msi->rank(), node);
    }
    int err = zmq_send(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
}

void Sender::send_mt(int node, MSI::MessageType mt) {
    int buf[1];
    buf[0] = mt;  // message type byte
    void* requester = peers()[node]->socket;
    guarantee(requester, "sanity");
    if (PrintMessageType) {
        printf("[node %d] -> [node %d](%d)\n", _msi->rank(), node, mt);
    }
    int err = zmq_send(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
}

void Sender::send_ptr(int node, MSI::MessageType mt, void *ptr) {
    int sbuf[1+sizeof(void*)/sizeof(int)];
    sbuf[0] = mt;
    void** p = reinterpret_cast<void**>(&sbuf[1]);
    *p = ptr;

    void* requester = peers()[node]->socket;
    zmq_send(requester, sbuf, sizeof(sbuf), 0);
}

void Sender::recv_mt(int node, MSI::MessageType mt) {
    int buf[1];
    void* requester = peers()[node]->socket;
    guarantee(requester, "sanity");
    int err = zmq_recv(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));

    if (PrintMessageType) {
        printf("[node %d] <- [node %d](%d)\n", _msi->rank(), node, buf[0]);
    }

    if (buf[0] != mt) {
        string err_msg = "Expecting " + std::to_string(mt) + " but got " + std::to_string(buf[0]);
        throw UnexpectedMessageTypeError(err_msg);
    }
}

MSI::MessageType Sender::recv_mt(int node) {
    int buf[1];
    void* requester = peers()[node]->socket;
    guarantee(requester, "sanity");
    int err = zmq_recv(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
    zpl("recv_mt: %d", buf[0]);
    return static_cast<MSI::MessageType>(buf[0]);
}

/**@brief Receive message with expected type and length
 *
 * @param node
 * @param mt: expected message type
 * @param data
 * @param len: expected len for data (not message)
 * @param flag
 */
void Sender::recv_short_data(int node, MSI::MessageType mt, void *data, size_t len, int flag) {
    int buf[MAX_MSG_LEN] = {0};

    void* requester = peers()[node]->socket;
    guarantee(requester, "sanity");
    int err = zmq_recv(requester, buf, sizeof(buf), flag);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));

    if (PrintMessageType) {
        printf("[node %d] <- [node %d](Data)\n", _msi->rank(), node);
    }

    if (buf[0] != mt) {
        string err_msg = "Expecting " + std::to_string(mt) + " but got " + std::to_string(buf[0]);
        throw UnexpectedMessageTypeError(err_msg);
    }

    int data_len = buf[1];
    guarantee(data_len == len, "data_len: %p, len: %d", data_len, len);
    zpl("data: %d", buf[2])
    memcpy(data, &buf[2], len);
}

void Sender::broadcast_invalidate(PtrRecord *pr) {
    for (auto h: peers()) {
        zpl("broadcast inv to %d", h.first)
        send_invalidate(h.first, pr);
        recv_mt(h.first, MSI::ACK_INVALIDATE);
        zpl("received inv ack from %d", h.first)
    }
}

//void Sender::broadcast_finished() {
//    for (auto h: peers()) {
//        int buf[1];
//        buf[0] = MSI::MSG_DONE;  // message type byte
//        void* requester = h.second->socket;
//        guarantee(requester, "sanity");
//        if (PrintMessageType) {
//            printf("[node %d] -> [node %d](%d)\n", _msi->rank(), h.first, buf[0]);
//        }
//        zmq_send(requester, buf, sizeof(buf), 0);
//        MSI::MessageType mt = recv_mt(h.first);
//        zpl("received fini ack %s from %d", _msi->strmt(mt), h.first);
//        //zmq_recv(requester, buf, sizeof(buf), 0);
//        //zpl("received fini ack %s from %d", _msi->strmt((MSI::MessageType)buf[0]), h.first);
//    }
//}


void Sender::broadcast_finished() {
    for (auto h: peers()) {
        zpl("broadcast fini to %d", h.first)

        int buf[2];
        buf[0] = MSI::MSG_DONE;  // message type byte
        buf[1] = _msi->rank();
        void* requester = peers()[h.first]->socket;
        int err = zmq_send(requester, buf, sizeof(buf), 0);
        guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));

        int rbuf[2];
        err= zmq_recv(h.second->socket, rbuf, sizeof(rbuf), 0);
        guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
        MSI::MessageType final_mt = (MSI::MessageType)rbuf[0];
        if (rbuf[0] == MSI::MSG_FETCH) {
            int id = rbuf[1];
            PtrRecord* pr = _msi->allocator()->get_id_record(id);
            zpl("requested data: %d", *((int*)pr->ptr))
            MSISend::send_short_data(h.second->socket, pr->ptr, pr->size, 0);
            final_mt = recv_mt(h.first);  // todo: this is not receiving ACK_FINISHED
        }
        guarantee(final_mt == MSI::ACK_DONE, "");
        zpl("received %s from %d", _msi->strmt(final_mt), h.first)
    }
}

int Sender::sem_create_remote(unsigned value) {
    int buf[2];
    buf[0] = MSI::SEM_CREATE;
    buf[1] = value;
    //buf[2] = pr->owner;
    void* requester = peers().at(0)->socket;
    //guarantee(requester, "node %d requester: %p", node, requester);
    if (PrintMessageType) {
        printf("[node %d] -> [node 0](SEM_Create): %d\n", _msi->rank(), value);
    }

    zmq_send(requester, buf, sizeof(buf), 0);
    //char rbuf[sizeof(int)+sizeof(void*)+sizeof(int)];  // plus an errno if failed
    char rbuf[3];
    zmq_recv(requester, rbuf, sizeof(rbuf), 0);
    guarantee(rbuf[0] == MSI::ACK_SEM_CREATE, "");

    // todo: add errno
    int sem_id = rbuf[1];

    if (PrintMessageType) {
        printf("[node %d] <- [node 0](ACK_SEM_Create): %p\n", _msi->rank(), sem_id);
    }
    return sem_id;
}

MSI::MessageType Sender::sem_wait_request(int sem_id) {
    //send_ptr(0, MSI::SEM_WAIT, p);
    int buf[3];
    buf[0] = MSI::SEM_WAIT;
    buf[1] = _msi->rank();
    buf[2] = sem_id;
    void* requester = peers().at(0)->socket;
    if (PrintMessageType) {
        printf("[node %d] -> [node 0](SEM_Wait): %d\n", _msi->rank(), sem_id);
    }

    int err = zmq_send(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));

    MSI::MessageType mt = recv_mt(0);
    return mt;
}

void Sender::sem_post_request(int sem_id) {
    int buf[3];
    buf[0] = MSI::SEM_POST;
    buf[1] = _msi->rank();
    buf[2] = sem_id;
    void* requester = peers().at(0)->socket;
    if (PrintMessageType) {
        printf("[node %d] -> [node 0](SEM_Post): %d\n", _msi->rank(), sem_id);
    }

    int err = zmq_send(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
    MSI::MessageType mt = recv_mt(0);
    guarantee(mt == MSI::SEM_POST_ACK, "");
}

void Sender::sem_signal_next_node(int node, int sem_id) {
    int buf[3];
    buf[0] = MSI::SEM_SIGNAL;
    buf[1] = sem_id;
    buf[2] = _signal_cnt++;

    printf("send sem_signal %d to node: %d\n", buf[2], node);
    Host* host = peers().at(node);
    void* requester = host->socket;
    //zpl("name %s, socket %p", host->name.c_str(), host->socket)
    for (auto it: peers()) {
        zpl("node %d %s, requester: %p", it.first, it.second->name.c_str(), it.second->socket)
    }

    int err = zmq_send(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq_send errono: %s", zmq_strerror(err));
    err = zmq_recv(requester, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq_recv errono: %s", zmq_strerror(err));
    //MSI::MessageType mt = recv_mt(node);
    guarantee(buf[0] == MSI::SEM_SIGNAL_ACK, "mt: %d", buf[0]);
}

void MSISend::send_short_data(void *socket, void *data, size_t len, int flag) {
    int buf[MAX_MSG_LEN] = {0};
    buf[0] = MSI::MSG_DATA;
    buf[1] = len;
    memcpy(&buf[2], data, len);
    zpl("respond data: %d (%d bytes)", *((int*)data), len)
    size_t bytes = 2*sizeof(int) + len;
    int err = zmq_send(socket, buf, bytes, flag);
    guarantee(err != -1, "zmq_recv errono: %s", zmq_strerror(err));
}

void MSIRecv::recv_short_data(void *socket, void *holder, size_t len, int flag) {
    int buf[MAX_MSG_LEN] = {0};
    int err = zmq_recv(socket, buf, sizeof(buf), flag);
    guarantee(err != -1, "zmq_recv errono: %s", zmq_strerror(err));
    int data_len = buf[1];
    guarantee(data_len == len, "data_len: %p, len: %d, msg type: %s, data: %d", data_len, len,
              SysDict::msi->strmt((MSI::MessageType)buf[0]), *(int*)(&buf[2]));
    zpl("recv short data: %d", buf[2])
    memcpy(holder, &buf[2], len);
}

MSI::MessageType MSIRecv::recv_mt(void* socket) {
    int buf[1];
    int err = zmq_recv(socket, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq_recv errono: %s", zmq_strerror(err));
    return static_cast<MSI::MessageType>(buf[0]);
}
