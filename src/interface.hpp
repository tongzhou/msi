//
// Created by tzhou on 11/5/17.
//

#ifndef MSI_INTERFACE_HPP
#define MSI_INTERFACE_HPP

#include <msi.hpp>


extern "C" void msi_init() {
    SysDict::init();
}
extern "C" void msi_destroy() {
    SysDict::destroy();
}

extern "C" void msi_Fork() {
    SysDict::msi->set_enabled();
}

extern "C" void* msi_malloc(size_t size) {
    return SysDict::msi->allocator()->malloc(size);
}

extern "C" void msi_pull(void* p) {
    SysDict::msi->pull(p);
}

extern "C" void msi_push(void* p) {
    SysDict::msi->push(p);
}

extern "C" int msi_sem_create(unsigned v) {
    return SysDict::msi->sem_create(v);
}

extern "C" int msi_sem_wait(int id) {
    return SysDict::msi->sem_wait(id);
}

extern "C" int msi_sem_post(int id) {
    return SysDict::msi->sem_post(id);
}

extern "C" int msi_rank() {
    //return msi->rank();
    return SysDict::msi->rank();
}


#endif //MSI_INTERFACE_HPP
