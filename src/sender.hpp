//
// Created by tzhou on 11/17/17.
//

#ifndef MSI_SENDER_HPP
#define MSI_SENDER_HPP


#include <utilities/macros.hpp>
#include <map>
#include "msi.hpp"

class Sender {
    MSI* _msi;
    std::map<int, Host*> _peers;
    int _signal_cnt;
public:
    Sender();
    void initialize();
    void finalize();
    void init_host_connections();
    void destroy_host_connections();

    std::map<int, Host*>& peers()                            { return _peers; }

    void send_fetch(PtrRecord* pr);
    void send_invalidate(int node, PtrRecord* pr);
    void send_ptr(int node, MSI::MessageType mt, void* ptr);
    void send_mt(int node, MSI::MessageType mt);
    MSI::MessageType recv_mt(int node);
    void recv_mt(int node, MSI::MessageType mt);
    void recv_short_data(int node, MSI::MessageType mt, void* data, size_t len, int flag=0);

    int sem_create_remote(unsigned value);
    MSI::MessageType sem_wait_request(int id);
    void sem_post_request(int id);
    void sem_signal_next_node(int node, int id);

    void broadcast_finished();
    void broadcast_invalidate(PtrRecord* pr);
};

class MSISend {
public:
    static void send_short_data(void* socket, void* data, size_t len, int flag);

};

class MSIRecv {
public:
    static void recv_short_data(void* socket, void* holder, size_t len, int flag);
    static MSI::MessageType recv_mt(void* socket);
};

#endif //MSI_SENDER_HPP
