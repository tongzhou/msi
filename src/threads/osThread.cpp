//
// Created by tzhou on 11/5/17.
//

#include <utilities/mutex.hpp>
#include "osThread.hpp"


OSThread::OSThread() {
    intialize();
}

OSThread::OSThread(OSThreadStartFunc start_proc, void *start_parm) {
    _start_proc = start_proc;
    _start_parm = start_parm;
    intialize();
}

void OSThread::intialize() {
    _startThread_lock = new Monitor();
}
