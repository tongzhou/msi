//
// Created by tzhou on 11/5/17.
//

#include <zmq.h>
#include <errno.h>
#include <utilities/macros.hpp>
#include <cassert>
#include <msi.hpp>
#include <utilities/types.hpp>
#include <cstring>
#include <utilities/flags.hpp>
#include "listenerThread.hpp"
#include "sender.hpp"
#include "osThread.hpp"


ListenerThread::ListenerThread(): Thread(Thread::listener_thread) {
    _responder = NULL;
    _msi = NULL;
    _finished_num = 0;
}

ListenerThread::~ListenerThread() {
    
}

void ListenerThread::initialize() {
    _msi = SysDict::msi;
    _host = _msi->hosts().at(_msi->rank())->name;
    init_socket();
}

void ListenerThread::finalize() {
    destroy_socket();
}

void ListenerThread::run() {
    while (!should_terminate()) {
        int buffer[MAX_MSG_LEN];
        int err = zmq_recv(_responder, buffer, MAX_MSG_LEN, 0);
        if (err >= 0) {
            //printf("Listener: received %s from %p\n", _msi->strmt(buffer[0]), _responder);
            /* reply different messages depending on the receiving message type */
            //process(buffer);
            switch (buffer[0]) {
                case MSI::MSG_DONE:
                    respond_finished(buffer);
                    break;
                case MSI::MSG_FETCH:
                    respond_fetch(buffer);
                    break;
                case MSI::MSG_INVALIDATE:
                    respond_invalidate(buffer);
                    break;
                case MSI::SEM_CREATE:
                    respond_sem_create(buffer);
                    break;
                case MSI::SEM_WAIT:
                    respond_sem_wait(buffer);
                    break;
                case MSI::SEM_POST:
                    respond_sem_post(buffer);
                    break;
                case MSI::SEM_SIGNAL:
                    respond_sem_signal(buffer);
                    break;
                case MSI::MSG_DATA:
                    respond_msg_data(buffer);
                    break;
            }
        }
        else {
            printf("zmq_recv errno: %s\n", zmq_strerror(errno));
        }
    }

    printf("ListenerThread %p is exiting...\n", pthread_self());
}

void ListenerThread::respond(char *buf, size_t len, int flag) {
    zmq_send(_responder, buf, len, flag);
}

void ListenerThread::do_done(char *buf) {
    _finished_num++;
    if (_finished_num == SysDict::msi->hosts().size()-1) {
        _should_terminate = true;
    }
    char sbuf = MSI::ACK_DONE;
    zmq_send(_responder, &sbuf, sizeof(sbuf), 0);  // send an ACK
}

void ListenerThread::do_fetch(char *buf) {

}

void ListenerThread::init_socket() {
    MSI* msi = SysDict::msi;
    _responder = zmq_socket(SysDict::msi->context(), ZMQ_REP);
    printf ("Thread " PID_FORMAT " is binding to %s\n", pthread_id(), _host.c_str());
    zpp(_responder)
    int rc = zmq_bind(_responder, _host.c_str());
    zpl("bind success")
    guarantee(rc == 0, "rc: %d, err: %s", rc, zmq_strerror(errno));
}

void ListenerThread::destroy_socket() {
    zmq_close(_responder);
}

void ListenerThread::respond_finished_fetch_data(PtrRecord* pr) {
    zpl("invalid state when other finishes")
    int buf[2];
    buf[0] = MSI::MSG_FETCH;  // message type byte
    buf[1] = pr->id;
    int err = zmq_send(_responder, buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
}

void ListenerThread::respond_msg_data(int* rbuf) {
    PtrRecord* pr = _pending_pr;
    int data_len = rbuf[1];
    guarantee(data_len == pr->size, "");
    memcpy(pr->ptr, &rbuf[2], data_len);
    zpl("pending pr: %p, data: %d, len: %d", pr, rbuf[2], rbuf[1])
    pr->state = PtrRecord::Shared;
    _pending_pr = NULL;
    respond_finished(rbuf);
}

void ListenerThread::respond_finished(int* rbuf) {
    int node = rbuf[1];
    for (auto it: _msi->allocator()->ptr_records()) {
        PtrRecord* pr = it.second;
        if (_msi->finished() && pr->state == PtrRecord::Invalid && pr->owner == node) {
//            zpl("invalid state when other finishes")
//            int buf[2];
//            buf[0] = MSI::MSG_FETCH;  // message type byte
//            buf[1] = pr->id;
//            int err = zmq_send(_responder, buf, sizeof(buf), 0);
            _pending_pr = pr;
            zpl("set _pending_pr %p", pr);
            respond_finished_fetch_data(pr);
            return;

            //MSIRecv::recv_short_data(_responder, pr->ptr, pr->size, 0);
        }
    }

    _finished_num++;
    if (_finished_num == _msi->hosts().size()-1) {
        set_should_terminate();
    }

    zpl("send ack_done")
    send_mt(MSI::ACK_DONE);
    //server_respond_short_data(MSI::ACK_DONE, NULL, 0, 0);  // send an ACK
    //zmq_send(_listener->responder(), &sbuf, sizeof(sbuf), 0);  // send an ACK
}

void ListenerThread::respond_fetch(int* rbuf) {
    int ptr_id = rbuf[1];
    PtrRecord* pr = _msi->allocator()->get_id_record(ptr_id);
    guarantee(pr, "");
    printf("to supply id %d: %d\n", ptr_id, *((int*)pr->ptr));
    if (pr->state == PtrRecord::Modified) {
        pr->state = PtrRecord::Shared;
    }
    respond_short_data(MSI::MSG_DATA, pr->ptr, pr->size, 0);
}

void ListenerThread::respond_invalidate(int* rbuf) {
    int ptr_id = rbuf[1];
    int owner = rbuf[2];
    printf("received invalidate request for pointer id %d\n", ptr_id);
    PtrRecord* pr = _msi->allocator()->get_id_record(ptr_id);
    pr->state = PtrRecord::Invalid;
    pr->owner = owner;
    zpl("set owner to %d", owner)
    guarantee(pr, "");
    send_mt(MSI::ACK_INVALIDATE);
}

void ListenerThread::send_mt(MSI::MessageType mt) {
    //zmq_send(responder(), &mt, sizeof(int), 0);
    int buf[1] = {0};
    buf[0] = mt;
    zpl("listener: send_mt %s", _msi->strmt(mt))
    int err = zmq_send(responder(), buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
}

void ListenerThread::respond_short_data(MSI::MessageType mt, void *data, size_t len, int flag) {
    int buf[MAX_MSG_LEN] = {0};
    buf[0] = mt;
    buf[1] = len;
    memcpy(&buf[2], data, len);
    zpl("respond data: %d", *((int*)data))
    size_t bytes = 2*sizeof(int) + len;
    int err = zmq_send(_responder, buf, bytes, flag);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
}

void ListenerThread::respond_sem_create(int* rbuf) {
    int init_value = rbuf[1];
    int sem_id = _msi->lock_manager()->create_semaphore(init_value);
    // MSISem* sem = new MSISem();
    // sem->magic = SEM_MAGIC;
    // sem->value = init_value;
    int sbuf[2];
    sbuf[0] = MSI::ACK_SEM_CREATE;
    sbuf[1] = sem_id;
    int err = zmq_send(responder(), sbuf, sizeof(sbuf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
    if (PrintMessageType) {
        printf("-> [node 0](ACK_SEM_Create): %d\n", sem_id);
    }
}

void ListenerThread::respond_sem_wait(int* rbuf) {
    int node = rbuf[1];
    int sem_id = rbuf[2];

    zpl("respond_sem_wait: %d", sem_id)

    MSI::MessageType mt = _msi->lock_manager()->do_sem_wait_request(node, sem_id);
    send_mt(mt);
}

void ListenerThread::respond_sem_post(int* rbuf) {
    int sem_id = rbuf[2];
    zpl("respond_sem_post: %d", sem_id)
    _msi->lock_manager()->do_sem_post_request(sem_id);
    send_mt(MSI::SEM_POST_ACK);
}

void ListenerThread::respond_sem_signal(int* rbuf) {
    int sem_id = rbuf[1];
    int signal_cnt = rbuf[2];
    int buf[1] = {0};
    buf[0] = MSI::SEM_SIGNAL_ACK;
    zpl("received sem_signal: %d", signal_cnt)
    int err = zmq_send(responder(), buf, sizeof(buf), 0);
    guarantee(err != -1, "zmq errono: %s", zmq_strerror(err));
    //send_mt(MSI::SEM_SIGNAL_ACK);
    _msi->resume_thread(sem_id);
}

int ListenerThread::get_int(int *rbuf, int offset) {
    return rbuf[offset];
}

void* ListenerThread::get_ptr(int *rbuf, int offset) {
    zpl("to get ptr")
    void** p = reinterpret_cast<void**>(&rbuf[offset]);
    return *p;
}
