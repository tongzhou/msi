//
// Created by tzhou on 11/5/17.
//

#ifndef MSI_LISTENERTHREAD_HPP
#define MSI_LISTENERTHREAD_HPP

#include "thread.hpp"
#include "utilities/macros.hpp"
#include "msi.hpp"

class MSI;
class ListenerThread: public Thread {
    MSI* _msi;
    string _host;
    void* _responder;
    int _finished_num;  // only used by master
    PtrRecord* _pending_pr;
public:
    ListenerThread();
    ~ListenerThread();

    void initialize();
    void finalize();
    //void set_msi(MSI* msi)                                { _msi = msi; };
    void run() override;

    void* responder()                                       { return _responder; }
    void respond(char* buf, size_t len, int flag=0);
    void do_done(char* buf);
    void do_fetch(char* buf);
    void process(char* buffer);

    /* The response methods are executed in listener thread */
    void respond_finished(int* rbuf);
    void respond_fetch(int* rbuf);
    void respond_invalidate(int* rbuf);
    void respond_finished_fetch_data(PtrRecord* pr);
    void respond_msg_data(int* rbuf);
    void respond_sem_create(int* rbuf);
    void respond_sem_wait(int* rbuf);
    void respond_sem_post(int* rbuf);
    void respond_sem_signal(int* rbuf);
    void send_mt(MSI::MessageType mt);
    void respond_short_data(MSI::MessageType mt, void* data, size_t len, int flag=0);
    int get_int(int* rbuf, int offset);  // offset is in ints
    void* get_ptr(int* rbuf, int offset);


    void init_socket();
    void destroy_socket();
};

#endif //MSI_LISTENERTHREAD_HPP
