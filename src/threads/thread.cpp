//
// Created by tzhou on 11/5/17.
//

#include <utilities/macros.hpp>
#include <threads/osThread.hpp>
#include <utilities/flags.hpp>
#include <utilities/mutex.hpp>
#include "thread.hpp"


// Threads
//#ifdef PTHREAD_OKAY  // pthread is assumed
std::map<pthread_t, Thread*> Threads::_threads_table;

void Threads::register_thread(Thread * thread) {
    _threads_table[thread->osthread()->pthread_id()] = thread;
}
//#endif

//JavaThread* Threads::_thread_list = NULL;
//int Threads::_number_of_threads = 0;
//
//jint Threads::create_vm(JavaVMInitArgs *args, bool *canTryAgain) {
//    // Initialize global data structures and create system classes in heap
//    init::vm_init_globals();
//
//    // Attach the main thread to this os thread
//    JavaThread* main_thread = new JavaThread();
//    main_thread->set_thread_state(JavaThread::_thread_in_vm);
//
//
//    if (!main_thread->set_as_starting_thread()) {
//        printf("Failed necessary internal allocation. Out of swap space");
//        delete main_thread;
//        *canTryAgain = false; // don't let caller call JNI_CreateJavaVM again
//        return JNI_ENOMEM;
//    }
//
//    // Initialize global modules such as heap
//    jint status = init::init_globals();
//
//    VMThread::create();
//    Thread* vmthread = VMThread::vm_thread();
//
//    os::create_thread(vmthread, os::vm_thread);
//
//    os::start_thread(vmthread);
//
//    /* for debug use */
//    int ret;
//    pthread_join(vmthread->osthread()->pthread_id(), (void**)&ret);
//}

//void Threads::add(JavaThread *p, bool force_daemon) {
//    p->set_next(_thread_list);
//    _thread_list = p;
//    _number_of_threads++;
//}


// Thread start routine for all newly created threads
static void *common_start_routine(Thread *thread) {
    Threads::register_thread(thread);
    OSThread *osthread = thread->osthread();

    Monitor *sync = osthread->startThread_lock();
    thread->do_initialization();  // do initialization work, parent thread is still blocked now

    // handshaking with parent thread
    {
        sync->lock();
        // notify wparent thread
        osthread->set_state(OSThread::INITIALIZED);
        sync->signal_all();
        sync->unlock();
    }

    // wait until os::start_thread()
    if (PrintThreadCreation) {
        printf("Thread " PID_FORMAT " has initialized, wait until os::start_thread()\n", osthread->pthread_id());
    }

    {
        sync->lock();
        while (osthread->state() == OSThread::INITIALIZED) {
            sync->wait();
            //pthread_cond_wait(sync->cond(), sync->mutex());
        }
        sync->unlock();
    }

    thread->run();
}

void Threads::start_thread(Thread *thread) {
    OSThread* osThread = thread->osthread();
    Monitor* sync = osThread->startThread_lock();
    sync->lock();
    osThread->set_state(OSThread::RUNNABLE);
    sync->signal();
    sync->unlock();
}

// Thread

Thread::Thread(ThreadType threadType) {
    OSThread* osthread = new OSThread();
    set_thread_type(threadType);
    osthread->set_state(OSThread::ALLOCATED);
    set_osthread(osthread);
    _should_terminate = false;

    pthread_t tid;
    int ret =  pthread_create(&tid, NULL, (void* (*)(void*)) common_start_routine, this);
    guarantee(ret == 0, "");
    if (PrintThreadCreation) {
        printf("created %s, pthread id: " PID_FORMAT "\n", thread_type_str(threadType), tid);
    }

    osthread->set_pthread_id(tid);

    // Wait until child thread is running
    /* the curly bracket is not necessary(as it is in HotSpot's code), just to make critical clearer */
    {
        Monitor* sync_with_child = osthread->startThread_lock();
        sync_with_child->lock();
        while (osthread->state() == OSThread::ALLOCATED) {
            sync_with_child->wait();
        }
        sync_with_child->unlock();
    }

}

Thread::~Thread() {

}

void Thread::run() {
    printf("Thread::run called: do nothing\n");
}

void Thread::do_initialization() {
    printf("Thread::do_initialization called: do nothing\n");
}

void Thread::start() {
    OSThread* osThread = osthread();
    Monitor* sync = osThread->startThread_lock();
    sync->lock();
    osThread->set_state(OSThread::RUNNABLE);
    sync->signal();
    sync->unlock();
}

void Thread::join() {
    int ret;
    pthread_join(osthread()->pthread_id(), (void**)&ret);
}

bool Thread::should_terminate() {
    return _should_terminate;
}

thread_id_t Thread::pthread_id() {
    return osthread()->pthread_id();
}

bool Thread::set_as_starting_thread() {
    // NOTE: this must be called inside the main thread.
    //return os::create_main_thread((JavaThread*)this);
}


//#ifdef PTHREAD_OKAY
Thread* Threads::get_pthread_by_id(pthread_t id) {
    if (_threads_table.find(id) == _threads_table.end()) {
        return NULL;
    }
    else {
        return _threads_table[id];
    }
}

Thread* Thread::current() {
    Thread* thread = Threads::get_pthread_by_id(pthread_self());
    guarantee(thread != NULL, "Thread::current(): thread is NULL");
    return thread;
}
