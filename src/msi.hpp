//
// Created by tzhou on 11/5/17.
//

#ifndef MSI_MSI_HPP
#define MSI_MSI_HPP

#include <map>
#include <utilities/macros.hpp>
#include <utilities/mutex.hpp>
#include <set>
#include <queue>

#define SEM_MAGIC 12345678

class ListenerThread;
class Sender;


struct PtrRecord {
    enum State {
        Invalid,
        Shared,
        Modified
    };
    void* ptr;
    int id;
    size_t size;
    State state;
    int owner;
};


struct MSISem {
    int magic;
    unsigned value;
    Mutex mutex;
};

class LockManager;

class Allocator {
    std::map<void*, PtrRecord*> _ptr_records;
    std::map<int, PtrRecord*> _id_records;
    int _alloc_id;
public:
    Allocator();
    void initialize();
    void finalize();

    void* malloc(size_t size);
    void add_ptr_record(void* p, size_t size);

    std::map<void*, PtrRecord*>& ptr_records()                      { return _ptr_records; }
    PtrRecord* get_ptr_record(void* p);
    PtrRecord* get_id_record(int id);
};

struct Host {
    string name;
    void* socket;
    Host(string n): name(n), socket(NULL) {}
};

class MSI {
    void* _context;
    int _rank;
    std::map<int, Host*> _hosts;
    Sender* _sender;
    ListenerThread* _listener;
    Allocator* _allocator;
    int _finished_num;  // only used by master
    bool _enabled;
    bool _finished;
    int _sem_cnt;

    /* locks */
    LockManager* _lock_manager;  // only on one node
    std::map<pthread_t, Monitor*> _execution_locks;  // pause or resume the execution thread, per thread
    int _exec_state;
public:
    enum MessageType {
        UNKNOWN,
        MSG_DONE,
        ACK_DONE,
        MSG_FETCH,
        MSG_DATA,
        MSG_INVALIDATE,
        ACK_INVALIDATE,
        SEM_CREATE,
        ACK_SEM_CREATE,
        SEM_WAIT,
        SEM_POST, // 10
        SEM_GRANTED,
        SEM_BLOCK,
        SEM_POST_ACK,
        SEM_SIGNAL,
        SEM_SIGNAL_ACK,
    };
    
    MSI();
    ~MSI();
    void initialize();
    void finalize();

    void* context()                                 { return _context; }
    int rank()                                      { return _rank; }
    int size()                                      { return _hosts.size(); }
    bool enabled()                                  { return _enabled; }
    bool finished()                                 { return _finished; }
    void set_enabled(bool v=true)                   { _enabled = v; }
    void set_exec_state(int v)                      { _exec_state = v; }

    const char* strmt(int mt);

    Sender* sender()                                { return _sender; }
    ListenerThread* listener()                      { return _listener; }
    Allocator* allocator()                          { return _allocator; }
    LockManager* lock_manager()                     { return _lock_manager; }
    Monitor* get_execution_lock();
    std::map<pthread_t, Monitor*>& execution_locks(){ return _execution_locks; }

    void pull(void* p);
    void push(void* p);
    void fetch(PtrRecord* pr);

    std::map<int, Host*>& hosts()                   { return _hosts; }

    void send_message(int node, MessageType mt, void* data, size_t len, int flag=0);


    void resume_thread(int sem_id);

    /* locks */
    int sem_create(unsigned value);
    int sem_destroy(void* sem);
    int sem_wait(int id);
    int sem_post(int id);
    bool sem_is_valid(void* sem);
};


class LockManager {
    MSI* _msi;
    int _sem_cnt;
    std::vector<MSISem*> _sems;
    std::queue<int> _blocked_nodes;
    Mutex* _blocked_queue_lock;
public:
    LockManager();
    void initialize();
    void finalize();
    int create_semaphore(unsigned v);
    MSI::MessageType do_sem_wait_request(int node, int sem_id);
    int do_sem_post_request(int sem_id);
    bool sem_is_valid(void* p);
    MSISem* get_sem(int p);
    void check_sem(void* p);
};

class SysDict {
public:
    static MSI* msi;

    static void init();
    static void destroy();
    static void* context()                          { return msi->context(); }
    static int rank()                               { return msi->rank(); }
};

struct Message {
    int type;  // 0: control; 1: shared data
    int len;
    void* data;
};




#endif //MSI_MSI_HPP
