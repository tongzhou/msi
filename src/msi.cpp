//
// Created by tzhou on 11/5/17.
//

#include <unistd.h>
#include <cstdlib>
#include <iostream>
//#include <algorithm>
#include <cstring>
#include <zmq.h>
#include <threads/listenerThread.hpp>
#include <utilities/flags.hpp>
#include <utilities/types.hpp>
#include "sender.hpp"
#include "msi.hpp"

MSI* SysDict::msi = NULL;

void SysDict::init() {
    //Errors::init();
    Errors::init();
    Flags::init();
    msi = new MSI();
    msi->initialize();  // has to be done after msi is assigned
}

void SysDict::destroy() {
    msi->finalize();
    delete msi;
}

Allocator::Allocator() {
    _alloc_id = 0;
}

void Allocator::initialize() {

}

void Allocator::finalize() {

}


/**@brief
 *
 * todo: Should only be allowed in the common section.
 *
 * @param size
 * @return
 */
void* Allocator::malloc(size_t size) {
//    if (enabled()) {
//        throw BadAllocError("Allocation from shared memory is only allowed in common sections!");
//    }

    void* p = std::malloc(size);

    add_ptr_record(p, size);
    return p;
}

void Allocator::add_ptr_record(void *p, size_t size) {
    PtrRecord* pr = new PtrRecord();
    pr->ptr = p;
    zpl("pr: %p, id: %d", pr, _alloc_id)
    pr->id = _alloc_id++;  // this id is forced to be consistent for all nodes
    pr->size = size;
    pr->state = PtrRecord::Shared;
    pr->owner = -1;
    _ptr_records[p] = pr;
    _id_records[pr->id] = pr;

    if (PrintMalloc) {
        printf("malloc ptr: %p, size: %d, id: %d\n", p, size, pr->id);
    }
}

PtrRecord* Allocator::get_ptr_record(void *p) {
    if (_ptr_records.find(p) == _ptr_records.end()) {
        return NULL;
    }
    else {
        return _ptr_records[p];
    }
}

PtrRecord* Allocator::get_id_record(int id) {
    if (_id_records.find(id) == _id_records.end()) {
        return NULL;
    }
    else {
        return _id_records[id];
    }
}

/**@brief Objects' creations are in MSI::initialize
 *
 */
MSI::MSI() {
    _context = NULL;
    _rank = -1;
    _sender = NULL;
    _listener = NULL;
    _allocator = NULL;
    _finished_num = 0;
    _finished = false;
    _enabled = false;
    _lock_manager = NULL;
    _sem_cnt = 0;
    _exec_state = 0;
}

MSI::~MSI() {
    
}

const char* MSI::strmt(int mt) {
    switch (mt) {
        case MSI::UNKNOWN:
            return "Unknown";
        case MSI::MSG_DONE:
            return "MSG_Done";
            break;
        case MSI::ACK_DONE:
            return "ACK_Done";
            break;
        case MSI::MSG_FETCH:
            return "MSG_Fetch";
            break;
        case MSI::MSG_DATA:
            return "MSG_Data";
            break;
        case MSI::MSG_INVALIDATE:
            return "MSG_Invalidate";
            break;
        case MSI::ACK_INVALIDATE:
            return "ACK_Invalidate";
            break;
        case MSI::SEM_CREATE:
            return "SEM_Create";
            break;
        case MSI::SEM_WAIT:
            return "SEM_Wait";
            break;
        case MSI::SEM_POST:
            return "SEM_Post";
            break;
        case MSI::SEM_POST_ACK:
            return "SEM_Post_ACK";
            break;
        case MSI::SEM_SIGNAL:
            return "SEM_Signal";
        case MSI::SEM_SIGNAL_ACK:
            return "SEM_Signal_ACK";
        case SEM_GRANTED:
            return "SEM_Granted";
        case SEM_BLOCK:
            return "SEM_Block";
        default:
            guarantee(0, "unrecoginzed: %d", mt);
    }
}

void MSI::initialize() {
    _context = zmq_ctx_new();
    char* comm_rank = getenv("MSIRank");
    if (!comm_rank) {
        std::cerr << "<msi>: Please specify MSIRank.\n";
        exit(1);
    }

    _rank = std::stoi(comm_rank);
    std::cout << "<msi>: Rank " << _rank << " says hello!\n";

    _hosts[0] = new Host("tcp://127.0.0.1:5555");
    _hosts[1] = new Host("tcp://127.0.0.1:5556");
    //_hosts[2] = new Host("tcp://127.0.0.1:5557");

    _lock_manager = new LockManager();
    _lock_manager->initialize();
    _execution_locks[pthread_self()] = new Monitor();
    _sender = new Sender();
    _sender->initialize();
    _listener = new ListenerThread();
    _listener->initialize();
    _listener->start();

    _allocator = new Allocator();
    _allocator->initialize();
    //sleep(1);
    //zpl("done MSI::initialize")
}

/**@brief
 * Wait for the communications to finish and close sockets of the sender
 * and the listener.
 *  
 * Master exit:
 * Master's listener needs a "finished" message from each slave node.
 * After that the listener thread can terminate.
 *
 * Slave exit:
 * Slave's send needs to send a "finished" message to master,
 * it then does zmq_ctx_destroy to wake up the listener.
 */
void MSI::finalize() {
//    if (rank() == 0) {
//        destroy_host_connections();
//
//        _listener->join();
//        delete _listener;
//        zmq_ctx_destroy(context());
//    }
//    else {
//        inform_master_finished();
//        destroy_host_connections();
//
//        _listener->set_should_terminate(true);  // to do: this is not atomic
//        zmq_ctx_destroy(context());  // wake up all blocking operations on all sockets and let them go to zmq_close
//        _listener->join();
//        delete _listener;
//    }
    zpl("in MSI::finalize")
    _finished = true;

    _sender->broadcast_finished();
    _listener->join();
    _listener->finalize();
    delete _listener;


    _sender->finalize();
    delete _sender;

    _allocator->finalize();
    delete _allocator;
    _lock_manager->finalize();
    delete _lock_manager;


    zmq_ctx_destroy(context());
}

void MSI::pull(void *p) {
    if (PrintPull) {
        printf("pull address %p\n", p);
    }
    PtrRecord* pr = _allocator->get_ptr_record(p);
    if (pr->state != PtrRecord::Invalid) {
        return;
    }
    else {
        guarantee(pr->owner != -1, "At invalid state, should have an owner");
        fetch(pr);
        pr->state = PtrRecord::Shared;
    }
}

/**@brief Fetch a most recent copy to local
 *
 * @param alloc_id
 */
void MSI::fetch(PtrRecord* pr) {
    guarantee(pr->state == PtrRecord::Invalid, "sanity");
    //send_ctrl_message(pr->owner, MSI::MSG_FETCH, &buf, sizeof(buf));
    _sender->send_fetch(pr);
    _sender->recv_short_data(pr->owner, MSI::MSG_DATA, pr->ptr, pr->size);
}


void MSI::push(void *p) {
    PtrRecord* pr = _allocator->get_ptr_record(p);
    guarantee(pr->state != PtrRecord::Invalid, "Shouldn't push an invalid value");
    if (pr->state == PtrRecord::Shared) {
        pr->owner = rank();
        _sender->broadcast_invalidate(pr);
    }
    else if (pr->state == PtrRecord::Modified) {
        /* do nothing */
    }
}

/**@brief
 *  
 * Return: return an opaque handle to the newly created semaphore if successful.
 * Otherwise, it shall return NULL and set errno.
 */
int MSI::sem_create(unsigned value) {
    int sem;
    if (rank() == 0) {  // directly invoke handler
        int assigned_id = _lock_manager->create_semaphore(value);
        guarantee(assigned_id == _sem_cnt, "");
    }
    sem = _sem_cnt++;
    printf("get new sem: %p\n", sem);
    return sem;
}

LockManager::LockManager() {
    _msi = NULL;
    _blocked_queue_lock = NULL;
}

void LockManager::initialize() {
    _msi = SysDict::msi;
    _sem_cnt = 0;
    _blocked_queue_lock = new Mutex();
}

void LockManager::finalize() {

}

int LockManager::create_semaphore(unsigned v) {
    MSISem* sem = new MSISem();
    sem->magic = SEM_MAGIC;
    sem->value = v;
    int sem_id = _sems.size();
    _sems.push_back(sem);
    return sem_id;
}

MSI::MessageType LockManager::do_sem_wait_request(int node, int sem_id) {
    MSI::MessageType ret;
    MSISem* sem = get_sem(sem_id);

    sem->mutex.lock();
    if (sem->value > 0) {
        sem->value--;
        ret = MSI::SEM_GRANTED;
        printf("sem value: %d, node %d should grant\n", sem->value, node);
    }
    else {
        guarantee(sem->value == 0, "");
        ret = MSI::SEM_BLOCK;
        //_blocked_queue_lock->lock();
        _blocked_nodes.push(node);
        //_blocked_queue_lock->unlock();
        printf("sem value: %d, node %d should block\n", sem->value, node);
    }
    sem->mutex.unlock();

    //printf("sem_wait request: %s\n", _msi->strmt(ret));

    return ret;
}

int LockManager::do_sem_post_request(int sem_id) {
    int next = -1;
    MSISem* sem = get_sem(sem_id);

    sem->mutex.lock();

//    _blocked_queue_lock->lock();
//    if (!_blocked_nodes.empty()) {
//
//    }
//    _blocked_queue_lock->unlock();

    zpd(_blocked_nodes.size())
    if (!_blocked_nodes.empty()) {
        next = _blocked_nodes.front();
        _blocked_nodes.pop();

        /* send wakeup msg to node next */

        if (_msi->rank() == next) {
            _msi->resume_thread(sem_id);
        }
        else {
            _msi->sender()->sem_signal_next_node(next, sem_id);
        }
    }
    else {
        sem->value++;
    }
    sem->mutex.unlock();
    //printf("sem_wait: %s\n", _msi->strmt(ret));

    return next;
}

/**@brief Send sem wait request.
 *
 * Executes in the worker thread.
 *
 * If on node 0:
 * Call locker manager's method in the same worker thread,
 * and get a command back that either grants or blocks.
 * Else:
 * Send a message to the locker manager and gets a message.
 *
 * After the worker thread receives the message, it either
 * blocks itself, or it acquires the semaphore and continue.
 *
 * @param p
 * @return
 */
int MSI::sem_wait(int sem_id) {
    MSI::MessageType cmd;
    Monitor* monitor = get_execution_lock();
    monitor->lock();
    _exec_state = 1;  // set state to block before the request
    monitor->unlock();
    if (rank() == 0) {  // directly invoke handler
        cmd = _lock_manager->do_sem_wait_request(rank(), sem_id);
    }
    else {
        cmd = _sender->sem_wait_request(sem_id);
    }

    printf("sem_wait %d: %s\n", sem_id, strmt(cmd));

    if (cmd == MSI::SEM_GRANTED) {
        monitor->lock();
        _exec_state = 0;
        monitor->unlock();
    }
    else {
        monitor->lock();
        while (_exec_state == 1) {
            monitor->wait();
        }
        monitor->unlock();
    }

    return 0;
}

int MSI::sem_post(int sem_id) {
    printf("sem_post: %d\n", sem_id);
    MSI::MessageType cmd;
    if (rank() == 0) {  // directly invoke handler
        _lock_manager->do_sem_post_request(sem_id);
    }
    else {
        _sender->sem_post_request(sem_id);
    }
    return 0;
}

Monitor* MSI::get_execution_lock() {
    auto tid = pthread_self();
    if (_execution_locks.find(tid) == _execution_locks.end()) {
        _execution_locks[tid] = new Monitor();
    }
    return _execution_locks.at(tid);
}

void MSI::resume_thread(int sem_id) {
    Monitor* lock = execution_locks().begin()->second;  // todo: this always just uses one lock
    zpl("resume")

    lock->lock();
    guarantee(_exec_state == 1, "The thread should be in paused state");
    _exec_state = 0;
    lock->signal();  // todo: change thread's state, use state as a condition
    lock->unlock();
}

bool MSI::sem_is_valid(void* p) {
    /* todo: should set errno on failure */
    if (!p) {
        return false;
    }

    MSISem* sem = static_cast<MSISem*>(p);
    if (sem->magic != SEM_MAGIC) {
        return false;
    }

    return true;
}

bool LockManager::sem_is_valid(void* p) {
    /* todo: should set errno on failure */
    if (!p) {
        return false;
    }

    MSISem* sem = static_cast<MSISem*>(p);
    if (sem->magic != SEM_MAGIC) {
        return false;
    }

    return true;
}

MSISem* LockManager::get_sem(int sem_id) {
    printf("get sem %d\n", sem_id);
    return _sems.at(sem_id);
}

void LockManager::check_sem(void* p) {
    /* todo: should set errno on failure */
    bool valid = true;
    if (!p) {
        valid = false;
    }

    MSISem* sem = static_cast<MSISem*>(p);
    if (sem->magic != SEM_MAGIC) {
        valid = false;
    }

    if (!valid) {
        throw InvalidSemaphoreError();
    }
}
