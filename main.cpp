#include <iostream>
#include <interface.hpp>


#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>

int sem = 0;

void sync_test(int* p) {
    msi_sem_wait(sem);
    printf("do my job...\n");
    sleep(0);
    msi_sem_post(sem);
}

void sync_inc(int* p) {
    msi_sem_wait(sem);
    msi_pull(p);
    printf("%d -> %d\n", *p, *p + 1);
    *p = *p + 1;
    printf("after %d\n", *p);
    msi_push(p);
    sleep(0);
    msi_sem_post(sem);
}

void test1() {
    msi_init();

    sem = msi_sem_create(1);
    int* p = (int*)msi_malloc(sizeof(int));
    *p = 0;
    int rank = msi_rank();

    const int loops = 5000;
    if (rank == 0) {
        for (int i = 0; i < loops; ++i) {
            sync_inc(p);
//            msi_pull(p);
//            printf("*p: %d\n", *p);
            //sleep(1);
        }
    }
    else {
        for (int i = 0; i < loops; ++i) {
            sync_inc(p);
//            msi_sem_wait(sem);
//            *p = *p + 1;
//            msi_push(p);
//            msi_sem_post(sem);
            //sleep(1);
        }
    }

    msi_destroy();
    printf("final: %d\n", *p);
}

void test2() {
    msi_init();

    int node_num = 2;
    sem = msi_sem_create(1);
    int* p = (int*)msi_malloc(sizeof(int)*node_num);
    p[0] = 0;
    p[1] = 0;
    int rank = msi_rank();

    if (rank == 0) {
        msi_sem_wait(sem);
        msi_pull(p);
        p[0] = 100;
        msi_push(p);
        msi_sem_post(sem);
    }
    else {
        msi_sem_wait(sem);
        msi_pull(p);
        p[1] = 101;
        msi_push(p);
        msi_sem_post(sem);
    }


    msi_destroy();
    printf("p[0]: %d, p[1]: %d\n", p[0], p[1]);
}

int main (int argc, char **argv) {
    test2();
    return 0;
}
