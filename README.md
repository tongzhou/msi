## MSI
MSI is a light-weight distributed memory sharing interface for C.

## Overview
MSI implements a memory allocator that allocate memory that is shared
by distributed machines. The following code snippets show the programming
model:

```c
#include <unistd.h>
#include <msi.h>

void test1() {
  // common section
  msi_init();
  int* p = msi_malloc(sizeof(int));  
  *p = 0;
  
  int rank = msi_rank();
  if (rank == 0) {  // node 0
    sleep(1);
    // get the most recent value of *p
    msi_pull(p); 
    printf("*p: %d\n", p);
  }
  else if (rank == 1) {  // node 1
    *p += 1;
    msi_push(p);
  }

  printf("final: %d\n", *p);  
  msi_destroy();  // all nodes join
}
```

The internal design please refer to http://web.eecs.utk.edu/~tzhou9/2017/msi.pdf